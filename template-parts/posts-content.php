<?php 
global $_AMB_PREFIX;
echo "\n<div class='$_AMB_PREFIX-post'>\n";
echo '<h1 class="'.$_AMB_PREFIX.'-post-title">'.get_the_title().'</h1>'.$_AMBn;
echo '<div class="'.$_AMB_PREFIX.'-post-content">'.$_AMBn;
the_content();
echo '</div>'.$_AMBn;
echo "</div>\n";