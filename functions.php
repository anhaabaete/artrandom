<?php

$_AMB_PREFIX = 'ArtRandom';
$_AMBn = "\n";
$amb_theme = wp_get_theme();


//Load alls scripts
function ArtRandom_load_scripts() {
    global $amb_theme;

    wp_enqueue_style( 'amb-style', 
        get_template_directory_uri().'/assets/css/main.css',array(),
        rand(10,100));

    wp_enqueue_script( 'amb-script', 
        get_template_directory_uri().'/assets/js/main.js', array('jquery'), 
        rand(10,100));
}

add_action( 'wp_enqueue_scripts', 'ArtRandom_load_scripts');


function amb_custom_background() { 
    $another_args = array(
        'default-color'      => '0000ff',
        'default-image'      => ''     
    );
    add_theme_support( 'custom-background', $another_args );
}

add_action('after_setup_theme','amb_custom_background');

