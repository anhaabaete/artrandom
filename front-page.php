<?php

get_header();

    $the_query = new WP_Query( array ( 'orderby' => 'rand', 'posts_per_page' => '1' ) );
    echo '<div class="'.$_AMB_PREFIX.'-post-box">';
    if ($the_query->have_posts()) 
    {
        $the_query->the_post(); // necessário para chamar fazer consumir o post se não loop forever
        get_template_part('template-parts/posts','content');
    }
    echo '</div>';


get_footer();